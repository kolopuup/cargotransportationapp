USE [master]
GO
/****** Object:  Database [CargoTransportationAppDataBase]    Script Date: 6/8/2022 8:03:42 AM ******/
CREATE DATABASE [CargoTransportationAppDataBase]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CargoTransportationAppDataBase', FILENAME = N'G:\sqk\MSSQL15.MSSQLSERVER02\MSSQL\DATA\CargoTransportationAppDataBase.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CargoTransportationAppDataBase_log', FILENAME = N'G:\sqk\MSSQL15.MSSQLSERVER02\MSSQL\DATA\CargoTransportationAppDataBase_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CargoTransportationAppDataBase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET ARITHABORT OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET RECOVERY FULL 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET  MULTI_USER 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'CargoTransportationAppDataBase', N'ON'
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET QUERY_STORE = OFF
GO
USE [CargoTransportationAppDataBase]
GO
/****** Object:  Table [dbo].[City]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Delivery]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Delivery](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PackageWeight] [float] NOT NULL,
	[PackageTypeId] [int] NOT NULL,
	[CityId] [int] NULL,
	[StreetId] [int] NULL,
	[HouseNumber] [int] NULL,
	[FlatNumber] [int] NULL,
	[SpeedTypeId] [int] NOT NULL,
	[PickUpPointId] [int] NULL,
	[FromWhomId] [int] NOT NULL,
	[ForWhomFullName] [nvarchar](150) NOT NULL,
	[Cost] [float] NOT NULL,
 CONSTRAINT [PK_Delivery] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeliveryId] [int] NOT NULL,
	[DeliveryManId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[TrackNumber] [nvarchar](50) NOT NULL,
	[EstimatedDeliveryTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PackageType]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Cost] [float] NOT NULL,
 CONSTRAINT [PK_PackageType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PickUpPoint]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PickUpPoint](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CityId] [int] NOT NULL,
	[StreetId] [int] NOT NULL,
	[HouseNumber] [int] NOT NULL,
 CONSTRAINT [PK_PickUpPoint] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SpeedType]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeedType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValueAdditionPercentage] [int] NOT NULL,
 CONSTRAINT [PK_SpeedType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Street]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Street](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CityId] [int] NOT NULL,
 CONSTRAINT [PK_Street] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 6/8/2022 8:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](150) NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](150) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
	[TelegramAccount] [nvarchar](150) NULL,
	[TelegramStatusId] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[City] ON 

INSERT [dbo].[City] ([Id], [Name]) VALUES (1, N'Kazan')
INSERT [dbo].[City] ([Id], [Name]) VALUES (2, N'Moscow')
INSERT [dbo].[City] ([Id], [Name]) VALUES (3, N'Saint-Peterburg')
SET IDENTITY_INSERT [dbo].[City] OFF
GO
SET IDENTITY_INSERT [dbo].[Delivery] ON 

INSERT [dbo].[Delivery] ([Id], [PackageWeight], [PackageTypeId], [CityId], [StreetId], [HouseNumber], [FlatNumber], [SpeedTypeId], [PickUpPointId], [FromWhomId], [ForWhomFullName], [Cost]) VALUES (3, 10, 3, 1, NULL, NULL, NULL, 2, 1, 3, N'Andrey Andreev', 172.5)
INSERT [dbo].[Delivery] ([Id], [PackageWeight], [PackageTypeId], [CityId], [StreetId], [HouseNumber], [FlatNumber], [SpeedTypeId], [PickUpPointId], [FromWhomId], [ForWhomFullName], [Cost]) VALUES (6, 11, 3, 1, NULL, NULL, NULL, 1, 3, 3, N'Gdgsd DGgsdg', 164.8)
INSERT [dbo].[Delivery] ([Id], [PackageWeight], [PackageTypeId], [CityId], [StreetId], [HouseNumber], [FlatNumber], [SpeedTypeId], [PickUpPointId], [FromWhomId], [ForWhomFullName], [Cost]) VALUES (8, 12, 3, NULL, NULL, NULL, NULL, 2, 4, 3, N'впыв ВЫп', 195.5)
INSERT [dbo].[Delivery] ([Id], [PackageWeight], [PackageTypeId], [CityId], [StreetId], [HouseNumber], [FlatNumber], [SpeedTypeId], [PickUpPointId], [FromWhomId], [ForWhomFullName], [Cost]) VALUES (11, 11, 3, NULL, NULL, NULL, NULL, 2, 4, 3, N'Dgdgsg Dgsdgsdg', 184)
INSERT [dbo].[Delivery] ([Id], [PackageWeight], [PackageTypeId], [CityId], [StreetId], [HouseNumber], [FlatNumber], [SpeedTypeId], [PickUpPointId], [FromWhomId], [ForWhomFullName], [Cost]) VALUES (2011, 12, 3, 2, 3, 12, 13, 2, NULL, 7, N'Nikolay Nikolaevich Nilkolaev', 195.5)
INSERT [dbo].[Delivery] ([Id], [PackageWeight], [PackageTypeId], [CityId], [StreetId], [HouseNumber], [FlatNumber], [SpeedTypeId], [PickUpPointId], [FromWhomId], [ForWhomFullName], [Cost]) VALUES (3011, 12, 1, 1, 4, 55, 11, 3, NULL, 7, N'Arseniy Arseniy Arseniy', 175.5)
INSERT [dbo].[Delivery] ([Id], [PackageWeight], [PackageTypeId], [CityId], [StreetId], [HouseNumber], [FlatNumber], [SpeedTypeId], [PickUpPointId], [FromWhomId], [ForWhomFullName], [Cost]) VALUES (3012, 12, 3, NULL, NULL, NULL, NULL, 3, 4, 3, N'Test Test', 229.5)
INSERT [dbo].[Delivery] ([Id], [PackageWeight], [PackageTypeId], [CityId], [StreetId], [HouseNumber], [FlatNumber], [SpeedTypeId], [PickUpPointId], [FromWhomId], [ForWhomFullName], [Cost]) VALUES (4011, 12, 3, 3, 5, 12, 12, 2, NULL, 3010, N'Kirill Nerchaner', 195.5)
SET IDENTITY_INSERT [dbo].[Delivery] OFF
GO
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([Id], [DeliveryId], [DeliveryManId], [StatusId], [TrackNumber], [EstimatedDeliveryTime]) VALUES (3, 3, 6, 3, N'2043EQV-Kazan', CAST(N'2022-06-08T19:26:22.380' AS DateTime))
INSERT [dbo].[Order] ([Id], [DeliveryId], [DeliveryManId], [StatusId], [TrackNumber], [EstimatedDeliveryTime]) VALUES (7, 6, 6, 3, N'2044CUB-Kazan', CAST(N'2022-06-08T17:48:27.010' AS DateTime))
INSERT [dbo].[Order] ([Id], [DeliveryId], [DeliveryManId], [StatusId], [TrackNumber], [EstimatedDeliveryTime]) VALUES (17, 11, 4, 3, N'2052XGA-PickUpPoint', CAST(N'2022-06-08T13:27:28.903' AS DateTime))
INSERT [dbo].[Order] ([Id], [DeliveryId], [DeliveryManId], [StatusId], [TrackNumber], [EstimatedDeliveryTime]) VALUES (18, 8, 4, 4, N'2052LVW-PickUpPoint', CAST(N'2022-06-08T14:05:37.337' AS DateTime))
INSERT [dbo].[Order] ([Id], [DeliveryId], [DeliveryManId], [StatusId], [TrackNumber], [EstimatedDeliveryTime]) VALUES (2011, 2011, 1010, 1, N'2034DEI-Moscow', CAST(N'2022-06-08T11:42:06.987' AS DateTime))
INSERT [dbo].[Order] ([Id], [DeliveryId], [DeliveryManId], [StatusId], [TrackNumber], [EstimatedDeliveryTime]) VALUES (2012, 3012, 1010, 1, N'2034TPO-PickUpPoint', CAST(N'2022-06-08T12:00:01.820' AS DateTime))
INSERT [dbo].[Order] ([Id], [DeliveryId], [DeliveryManId], [StatusId], [TrackNumber], [EstimatedDeliveryTime]) VALUES (2013, 3011, 4, 3, N'2034TKS-Kazan', CAST(N'2022-06-08T12:57:33.063' AS DateTime))
INSERT [dbo].[Order] ([Id], [DeliveryId], [DeliveryManId], [StatusId], [TrackNumber], [EstimatedDeliveryTime]) VALUES (3011, 4011, 4, 4, N'2034CCI-Saint-Peterburg', CAST(N'2022-06-08T19:51:53.547' AS DateTime))
SET IDENTITY_INSERT [dbo].[Order] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderStatus] ON 

INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (1, N'Принят')
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (2, N'Процесс доставки')
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (3, N'Доставлен')
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (4, N'Получатель отказался')
SET IDENTITY_INSERT [dbo].[OrderStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[PackageType] ON 

INSERT [dbo].[PackageType] ([Id], [Name], [Cost]) VALUES (1, N'Конверт', 10)
INSERT [dbo].[PackageType] ([Id], [Name], [Cost]) VALUES (2, N'Коробка', 35)
INSERT [dbo].[PackageType] ([Id], [Name], [Cost]) VALUES (3, N'Ящик', 50)
INSERT [dbo].[PackageType] ([Id], [Name], [Cost]) VALUES (4, N'Контейнер', 150)
SET IDENTITY_INSERT [dbo].[PackageType] OFF
GO
SET IDENTITY_INSERT [dbo].[PickUpPoint] ON 

INSERT [dbo].[PickUpPoint] ([Id], [CityId], [StreetId], [HouseNumber]) VALUES (1, 1, 1, 2)
INSERT [dbo].[PickUpPoint] ([Id], [CityId], [StreetId], [HouseNumber]) VALUES (3, 1, 4, 55)
INSERT [dbo].[PickUpPoint] ([Id], [CityId], [StreetId], [HouseNumber]) VALUES (4, 2, 2, 2)
INSERT [dbo].[PickUpPoint] ([Id], [CityId], [StreetId], [HouseNumber]) VALUES (5, 3, 5, 13)
INSERT [dbo].[PickUpPoint] ([Id], [CityId], [StreetId], [HouseNumber]) VALUES (6, 2, 3, 55)
INSERT [dbo].[PickUpPoint] ([Id], [CityId], [StreetId], [HouseNumber]) VALUES (7, 3, 6, 12)
INSERT [dbo].[PickUpPoint] ([Id], [CityId], [StreetId], [HouseNumber]) VALUES (1007, 1, 1, 12)
SET IDENTITY_INSERT [dbo].[PickUpPoint] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([Id], [Name]) VALUES (1, N'Admin')
INSERT [dbo].[Role] ([Id], [Name]) VALUES (2, N'DeliveryMan')
INSERT [dbo].[Role] ([Id], [Name]) VALUES (3, N'SeniorDeliveryMan')
INSERT [dbo].[Role] ([Id], [Name]) VALUES (4, N'Client')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[SpeedType] ON 

INSERT [dbo].[SpeedType] ([Id], [Name], [ValueAdditionPercentage]) VALUES (1, N'Economy', 3)
INSERT [dbo].[SpeedType] ([Id], [Name], [ValueAdditionPercentage]) VALUES (2, N'Premium', 15)
INSERT [dbo].[SpeedType] ([Id], [Name], [ValueAdditionPercentage]) VALUES (3, N'Business', 35)
SET IDENTITY_INSERT [dbo].[SpeedType] OFF
GO
SET IDENTITY_INSERT [dbo].[Street] ON 

INSERT [dbo].[Street] ([Id], [Name], [CityId]) VALUES (1, N'Bauman', 1)
INSERT [dbo].[Street] ([Id], [Name], [CityId]) VALUES (2, N'Bolshaya Krasnaya', 2)
INSERT [dbo].[Street] ([Id], [Name], [CityId]) VALUES (3, N'Luzhniki', 2)
INSERT [dbo].[Street] ([Id], [Name], [CityId]) VALUES (4, N'Pushkina', 1)
INSERT [dbo].[Street] ([Id], [Name], [CityId]) VALUES (5, N'Saharova', 3)
INSERT [dbo].[Street] ([Id], [Name], [CityId]) VALUES (6, N'Centeralnaya', 3)
SET IDENTITY_INSERT [dbo].[Street] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (1, N'Admin Admin', N'1', N'1', N'gdsgs@mail.ru', N'+5325235', 1, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (3, N'Alexander Gujy', N'3', N'3', N'gdgsg@magsd.ru', N'+534253623', 4, N'911331089', 1)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (4, N'Delivery Mang', N'2', N'2', N'GDGSGSD', N'234234', 2, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (6, N'Aqua Man', N'4', N'4', N'dgsdg@mail.ru', N'+64634634', 2, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (7, N'Aboba Bob', N'5', N'5', N'dgsdg', N'dgsg', 4, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (8, N'Jerome George', N'5', N'5', N'dgdg', N'sdgsdg', 4, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (1010, N'Daniel Aldridge', N'6', N'6', N'gdgsdgrsgf@mail.ru', N'+46346346346', 2, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (1011, N'Test Test Admin', N'7', N'7', N'test', N'test', 1, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (1012, N'DSgdsgjkjsdklg', N'Dgsdgdsg', N'SDgdgdsfds', N'DSgdgsdg', N'+623532434', 4, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (2010, N'OlegVaimer', N'oleg', N'oleg123', N'oleg123@mail.ru', N'+79871363634', 4, NULL, NULL)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [Email], [PhoneNumber], [RoleId], [TelegramAccount], [TelegramStatusId]) VALUES (3010, N'ArseniyYashonkov', N'123', N'123', N'test@mail.ru', N'+79871865560', 4, NULL, NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Delivery]  WITH CHECK ADD  CONSTRAINT [FK_Delivery_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([Id])
GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_City]
GO
ALTER TABLE [dbo].[Delivery]  WITH CHECK ADD  CONSTRAINT [FK_Delivery_PackageType] FOREIGN KEY([PackageTypeId])
REFERENCES [dbo].[PackageType] ([Id])
GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_PackageType]
GO
ALTER TABLE [dbo].[Delivery]  WITH CHECK ADD  CONSTRAINT [FK_Delivery_PickUpPoint] FOREIGN KEY([PickUpPointId])
REFERENCES [dbo].[PickUpPoint] ([Id])
GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_PickUpPoint]
GO
ALTER TABLE [dbo].[Delivery]  WITH CHECK ADD  CONSTRAINT [FK_Delivery_SpeedType] FOREIGN KEY([SpeedTypeId])
REFERENCES [dbo].[SpeedType] ([Id])
GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_SpeedType]
GO
ALTER TABLE [dbo].[Delivery]  WITH CHECK ADD  CONSTRAINT [FK_Delivery_Street] FOREIGN KEY([StreetId])
REFERENCES [dbo].[Street] ([Id])
GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_Street]
GO
ALTER TABLE [dbo].[Delivery]  WITH CHECK ADD  CONSTRAINT [FK_Delivery_User] FOREIGN KEY([FromWhomId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_User]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Delivery] FOREIGN KEY([DeliveryId])
REFERENCES [dbo].[Delivery] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Delivery]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[OrderStatus] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderStatus]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_User] FOREIGN KEY([DeliveryManId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_User]
GO
ALTER TABLE [dbo].[PickUpPoint]  WITH CHECK ADD  CONSTRAINT [FK_PickUpPoint_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickUpPoint] CHECK CONSTRAINT [FK_PickUpPoint_City]
GO
ALTER TABLE [dbo].[PickUpPoint]  WITH CHECK ADD  CONSTRAINT [FK_PickUpPoint_Street] FOREIGN KEY([StreetId])
REFERENCES [dbo].[Street] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickUpPoint] CHECK CONSTRAINT [FK_PickUpPoint_Street]
GO
ALTER TABLE [dbo].[Street]  WITH CHECK ADD  CONSTRAINT [FK_Street_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([Id])
GO
ALTER TABLE [dbo].[Street] CHECK CONSTRAINT [FK_Street_City]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
USE [master]
GO
ALTER DATABASE [CargoTransportationAppDataBase] SET  READ_WRITE 
GO
