﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTransportationDesktopApp.Model
{
    partial class Delivery
    {
        public string PlaceOfIssue
        {
            get
            {
                if (PickUpPointId != null)
                {
                    return $"Pick up point: {PickUpPoint.City.Name} {PickUpPoint.Street.Name} {PickUpPoint.HouseNumber}";
                }
                return $"{City.Name} {Street.Name} {HouseNumber} {FlatNumber}";
            }
            set
            {

            }
        }
    }
}
