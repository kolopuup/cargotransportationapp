﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTransportationDesktopApp.Model
{
    partial class User
    {
        public int SuccessOrderCount
        {
            get
            {
                return this.Order.Where(c => c.OrderStatus.Name == "Принят").Count();
            }
            set
            {

            }
        }
        public int DeliveryOrderCount
        {
            get
            {
                return this.Order.Where(c => c.OrderStatus.Name == "Доставлен").Count();
            }
            set
            {

            }
        }
        public int DeniedOrderCount
        {
            get
            {
                return this.Order.Where(c => c.OrderStatus.Name == "Получатель отказался").Count();
            }
            set
            {

            }
        }
        public int ProcessOrderCount
        {
            get
            {
                return this.Order.Where(c => c.OrderStatus.Name == "Процесс доставки").Count();
            }
            set
            {

            }
        }
    }
}
