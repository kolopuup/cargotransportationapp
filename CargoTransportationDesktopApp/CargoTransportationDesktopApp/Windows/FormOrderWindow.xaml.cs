﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Http;

namespace CargoTransportationDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for FormOrderWindow.xaml
    /// </summary>
    public partial class FormOrderWindow : Window
    {
        Model.Delivery postDelivery;
        Model.Order order = new Model.Order();
        public FormOrderWindow(Model.Delivery delivery)
        {
            InitializeComponent();
            postDelivery = delivery;
            postDelivery.Cost = CalculateCost();

            order.Delivery = postDelivery;
            order.EstimatedDeliveryTime = CalculateEstimatedDeliveryTime();
            order.TrackNumber = GenerateTrackNumber();

            this.DataContext = order;
        }

        private void BSelectDeliveryMan_Click(object sender, RoutedEventArgs e)
        {
            Windows.SelectDeliveryManWindow selectDeliveryManWindow = new SelectDeliveryManWindow();
            selectDeliveryManWindow.ShowDialog();
            if (selectDeliveryManWindow.DGDeliveryMan.SelectedItem != null)
            {
                order.User = (Model.User)selectDeliveryManWindow.DGDeliveryMan.SelectedItem;
                this.DataContext = null;
                this.DataContext = order;
            }
        }
        string letters = "QWERTYUIOPASDFGHJKLZXCVBNM";
        private double CalculateCost()
        {
            double cost = 0;
            cost += postDelivery.PackageType.Cost;
            cost += postDelivery.PackageWeight * 10;
            cost += (double)(cost * postDelivery.SpeedType.ValueAdditionPercentage / 100);
            return cost;
        }
        private DateTime CalculateEstimatedDeliveryTime()
        {
            Random random = new Random();
            DateTime dateTime = new DateTime();
            dateTime = DateTime.Now;
            if (postDelivery.SpeedType.Id == 1)
            {
                dateTime.AddDays((double)random.Next(35, 45));
            }
            if (postDelivery.SpeedType.Id == 2)
            {
                dateTime.AddDays((double)random.Next(15, 20));
            }
            if (postDelivery.SpeedType.Id == 3)
            {
                dateTime.AddDays((double)random.Next(4, 6));
            }
            return dateTime;
        }
        private string GenerateTrackNumber()
        {

            string trackNumber = "";
            Random random = new Random();
            string forLetters = "";
            for (int i = 0; i < 3; i++)
            {
                forLetters += letters[random.Next(0, letters.Length)];
            }
            trackNumber += (DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day).ToString() + forLetters;
            if (postDelivery.CityId == null)
            {
                trackNumber += "-" + "PickUpPoint";
            }
            else
            {
                trackNumber += "-" + postDelivery.City.Name;
            }
            if(MainWindow.db.Order.FirstOrDefault(c=>c.TrackNumber == trackNumber) != null)
            {
                GenerateTrackNumber();
            }
            return trackNumber;
        }
        private async void BCreateOrder_Click(object sender, RoutedEventArgs e)
        {
            if (order.User is null)
            {
                MessageBox.Show("Выберите курьера");
                return;
            }
            if (MessageBox.Show("Подтверждаете введённые данные?", "Подтверждение", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                order.StatusId = 1;
                MainWindow.db.Order.Add(order);
                MainWindow.db.SaveChanges();
                if(order.Delivery.User.TelegramAccount != null)
                {
                    HttpResponseMessage httpResponseMessage = await MainWindow.client.PostAsync($"{MainWindow.WebApiUrl}/api/PostNewOrder?OrderId={order.Id}", null);
                    MessageBox.Show("Заказ успешно подтверждён\nОтправляем уведомление клиенту");
                }
                else
                {
                    MessageBox.Show("Заказ успешно подтверждён\nУ клиента не привязан телеграм аккаунт");
                }
                this.Close();
            }
        }
    }
}
