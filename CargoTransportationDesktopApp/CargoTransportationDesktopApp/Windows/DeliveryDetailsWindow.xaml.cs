﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for DeliveryDetailsWindow.xaml
    /// </summary>
    public partial class DeliveryDetailsWindow : Window
    {
        Model.Delivery postDelivery;
        public DeliveryDetailsWindow(Model.Delivery delivery)
        {
            InitializeComponent();
            postDelivery = delivery;
            double cost = 0;
            cost += postDelivery.PackageType.Cost;
            cost += postDelivery.PackageWeight * 10;
            cost += (double)(cost * postDelivery.SpeedType.ValueAdditionPercentage / 100);
            postDelivery.Cost = cost;
            this.DataContext = postDelivery;
        }
    }
}
