﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for SelectDeliveryManWindow.xaml
    /// </summary>
    public partial class SelectDeliveryManWindow : Window
    {
        public SelectDeliveryManWindow()
        {
            InitializeComponent();
            DGDeliveryMan.ItemsSource = MainWindow.db.User.Where(c => c.Role.Name == "DeliveryMan").ToList();
        }

        private void DGDeliveryMan_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(DGDeliveryMan.SelectedItem is null)
            {
                MessageBox.Show("Выберите курьера");
                return;
            }
            this.Close();
        }
    }
}
