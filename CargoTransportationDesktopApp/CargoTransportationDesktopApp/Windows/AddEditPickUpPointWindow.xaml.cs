﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for AddEditPickUpPointWindow.xaml
    /// </summary>
    public partial class AddEditPickUpPointWindow : Window
    {
        Model.PickUpPoint postPoint;
        public AddEditPickUpPointWindow(Model.PickUpPoint point)
        {
            InitializeComponent();
            postPoint = point;

            CBCity.ItemsSource = MainWindow.db.City.ToList();
            this.DataContext = postPoint;
        }
        private void BSave_Click(object sender, RoutedEventArgs e)
        {
            if(CBCity.SelectedItem is null)
            {
                MessageBox.Show("Select City");
                return;
            }
            if(CBStreet.SelectedItem is null)
            {
                MessageBox.Show("Select street");
                return;
            }
            if(postPoint.Id == 0)
            {
                MainWindow.db.PickUpPoint.Add(postPoint);
            }
            MainWindow.db.SaveChanges();
            MessageBox.Show("Saved");
            this.Close();
        }

        private void CBCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CBStreet.ItemsSource = null;
            CBStreet.ItemsSource = MainWindow.db.Street.Where(c => c.City.Id == ((Model.City)CBCity.SelectedItem).Id).ToList();
        }
    }
}
