﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for AddEditClientWindow.xaml
    /// </summary>
    public partial class AddEditClientWindow : Window
    {
        Model.User postUser;
        public AddEditClientWindow(Model.User user)
        {
            InitializeComponent();
            postUser = user;
            CBRole.ItemsSource = MainWindow.db.Role.ToList();
            if(postUser.Role == null)
            {
                CBRole.SelectedIndex = 0;
            }
            this.DataContext = postUser;
        }

        private void BSave_Click(object sender, RoutedEventArgs e)
        {
            if(postUser.Id == 0)
            {
                MainWindow.db.User.Add(postUser);
            }
            MainWindow.db.SaveChanges();
            MessageBox.Show("Saved");
            this.Close();
        }
    }
}
