﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for MainMenuPage.xaml
    /// </summary>
    public partial class MainMenuPage : Page
    {
        public MainMenuPage()
        {
            InitializeComponent();
        }

        private void BClient_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ClientPage());
        }

        private void BOrder_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new OrderListPage());
        }

        private void BPickUpPoint_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PickUpPointsList());
        }

        private void BDelivery_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new DeliveryListPage());
        }

        private void BReport_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new DeliveryReportPage());
        }

        private void BCreateDelivery_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreateDeliveryPage());
        }
    }
}
