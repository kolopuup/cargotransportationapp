﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for PickUpPointsList.xaml
    /// </summary>
    public partial class PickUpPointsList : Page
    {
        public PickUpPointsList()
        {
            InitializeComponent();
            Refresh();
            
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            Windows.AddEditPickUpPointWindow addEditPickUpPointWindow = new Windows.AddEditPickUpPointWindow(new Model.PickUpPoint());
            addEditPickUpPointWindow.ShowDialog();
            Refresh();
        }

        private void BEdit_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate()) return;
            Windows.AddEditPickUpPointWindow addEditPickUpPointWindow = new Windows.AddEditPickUpPointWindow((Model.PickUpPoint)DGPickUpPoint.SelectedItem);
            addEditPickUpPointWindow.ShowDialog();
            Refresh();
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate()) return;
            MainWindow.db.PickUpPoint.Remove((Model.PickUpPoint)DGPickUpPoint.SelectedItem);
            MainWindow.db.SaveChanges();
            Refresh();
        }
        private bool Validate()
        {
            if(DGPickUpPoint.SelectedItem is null)
            {
                MessageBox.Show("Select Item");
                return false;
            }
            return true;
        }
        private void Refresh()
        {
            DGPickUpPoint.ItemsSource = null;
            DGPickUpPoint.ItemsSource = MainWindow.db.PickUpPoint.ToList();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Refresh();
        }
    }
}
