﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for OrderListPage.xaml
    /// </summary>
    public partial class OrderListPage : Page
    {
        public OrderListPage()
        {
            InitializeComponent();
            CBStatusFilter.ItemsSource = MainWindow.db.OrderStatus.ToList();
            Refresh();
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void Refresh()
        {
            List<Model.Order> orderList = MainWindow.db.Order.ToList();
            string statusName = ((Model.OrderStatus)CBStatusFilter.SelectedItem).Name;
            orderList = orderList.Where(c => c.OrderStatus.Name == statusName).ToList();
            DGOrderList.ItemsSource = null;
            DGOrderList.ItemsSource = orderList;
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if(DGOrderList.SelectedItem is null)
            {
                MessageBox.Show("Выберите элемент");
                return;
            }
            MainWindow.db.Order.Remove((Model.Order)DGOrderList.SelectedItem);
            MainWindow.db.SaveChanges();
            Refresh();
        }

        private void CBStatusFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Refresh();
        }
    }
}
