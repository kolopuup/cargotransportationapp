﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for ClientPage.xaml
    /// </summary>
    public partial class ClientPage : Page
    {
        public ClientPage()
        {
            InitializeComponent();
            Refresh();
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            Windows.AddEditClientWindow addEditClientWindow = new Windows.AddEditClientWindow(new Model.User());
            addEditClientWindow.ShowDialog();
            Refresh();
        }

        private void BEdit_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate()) return;
            Windows.AddEditClientWindow addEditClientWindow = new Windows.AddEditClientWindow((Model.User)DGClientList.SelectedItem);
            addEditClientWindow.ShowDialog();
            Refresh();
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate()) return;
            MainWindow.db.PickUpPoint.Remove((Model.PickUpPoint)DGClientList.SelectedItem);
            MainWindow.db.SaveChanges();
            Refresh();
        }
        private bool Validate()
        {
            if (DGClientList.SelectedItem is null)
            {
                MessageBox.Show("Select Item");
                return false;
            }
            return true;
        }
        private void Refresh()
        {
            DGClientList.ItemsSource = null;
            DGClientList.ItemsSource = MainWindow.db.User.ToList();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Refresh();
        }
    }
}
