﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for DeliveryListPage.xaml
    /// </summary>
    public partial class DeliveryListPage : Page
    {
        public DeliveryListPage()
        {
            InitializeComponent();
            Refresh();
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void BFormOrder_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate()) return;
            Windows.FormOrderWindow formOrderWindow = new Windows.FormOrderWindow((Model.Delivery)DGDelivery.SelectedItem);
            formOrderWindow.ShowDialog();
            Refresh();
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate()) return;
            
        }
        private bool Validate()
        {
            if(DGDelivery.SelectedItem is null)
            {
                MessageBox.Show("Выберите элемент");
                return false;
            }
            return true;
        }
        private void BDetails_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate()) return;
            Windows.DeliveryDetailsWindow deliveryDetailsWindow = new Windows.DeliveryDetailsWindow((Model.Delivery)DGDelivery.SelectedItem);
            deliveryDetailsWindow.ShowDialog();
        }
        private void Refresh()
        {
            DGDelivery.ItemsSource = null;
            DGDelivery.ItemsSource = MainWindow.db.Delivery.Where(c=>c.Order.Count == 0).ToList();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Refresh();
        }
    }
}
