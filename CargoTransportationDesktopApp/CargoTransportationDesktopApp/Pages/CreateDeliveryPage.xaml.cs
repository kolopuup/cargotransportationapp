﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for CreateDeliveryPage.xaml
    /// </summary>
    public partial class CreateDeliveryPage : Page
    {
        Model.Delivery delivery = new Model.Delivery();
        public CreateDeliveryPage()
        {
            InitializeComponent();
            CBCity.ItemsSource = MainWindow.db.City.ToList();
            CBSpeedType.ItemsSource = MainWindow.db.SpeedType.ToList();
            CBPackageType.ItemsSource = MainWindow.db.PackageType.ToList();
            CBPickUpPoint.ItemsSource = MainWindow.db.PickUpPoint.ToList();
            DGFromWhom.ItemsSource = MainWindow.db.User.Where(c => c.RoleId == 4).ToList();
            this.DataContext = delivery;
        }

        private void RBAddress_Click(object sender, RoutedEventArgs e)
        {
            delivery.PickUpPoint = null;
            CBPickUpPoint.SelectedItem = null;
            SPAddress.IsEnabled = true;
            SPPickUpPoint.IsEnabled = false;
        }

        private void RBPickUpPoint_Click(object sender, RoutedEventArgs e)
        {
            CBStreet.SelectedItem = null;
            CBCity.SelectedItem = null;
            TBFlatNumber.Text = null;
            TBHouseNumber.Text = null;
            delivery.CityId = null;
            delivery.StreetId = null;
            delivery.HouseNumber = null;
            delivery.FlatNumber = null;
            SPAddress.IsEnabled = false;
            SPPickUpPoint.IsEnabled = true;
        }

        private void CBCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Model.City city = (Model.City)CBCity.SelectedItem;
                CBStreet.ItemsSource = null;
                CBStreet.ItemsSource = MainWindow.db.Street.Where(c => c.CityId == city.Id).ToList();
            }
            catch
            {

            }
        }

        private void BCreate_Click(object sender, RoutedEventArgs e)
        {
            delivery.Cost = CalculateCost();
            if (MessageBox.Show("Warning", $"Create delivery?\nCost: {delivery.Cost}", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                MainWindow.db.Delivery.Add(delivery);
                MainWindow.db.SaveChanges();
                MessageBox.Show("Заявка ушла на обработку");
                NavigationService.GoBack();
            }
        }
        private double CalculateCost()
        {
            double cost = 0;
            cost += delivery.PackageType.Cost;
            cost += delivery.PackageWeight * 10;
            cost += (double)(cost * delivery.SpeedType.ValueAdditionPercentage / 100);
            return cost;
        }
        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void TBSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            DGFromWhom.ItemsSource = null;
            if (TBSearch.Text is "")
            {
                DGFromWhom.ItemsSource = MainWindow.db.User.Where(c => c.RoleId == 4).ToList();
                return;
            }
            DGFromWhom.ItemsSource = MainWindow.db.User.Where(c => c.RoleId == 4 && c.FullName.ToLower().Contains(TBSearch.Text.ToLower())).ToList();
        }
    }
}
