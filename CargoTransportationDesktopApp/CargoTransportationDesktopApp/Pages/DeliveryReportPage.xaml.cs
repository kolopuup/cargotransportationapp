﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DataVis = System.Windows.Forms.DataVisualization;

namespace CargoTransportationDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for DeliveryReportPage.xaml
    /// </summary>
    public partial class DeliveryReportPage : Page
    {
        public DeliveryReportPage()
        {
            InitializeComponent();
            DGDeliveryMan.ItemsSource = MainWindow.db.User.Where(c => c.RoleId == 2).ToList();
            TBDeliveryMan.Text = MainWindow.db.User.Where(c => c.RoleId == 2).Count().ToString();
            TBOrder.Text = MainWindow.db.Order.Count().ToString();
            TBOrderSuccess.Text = MainWindow.db.Order.Where(c => c.StatusId == 3).Count().ToString();
            CBChartData_SelectionChanged(null, null);
        }
        static List<Model.Order> orders = MainWindow.db.Order.ToList();
        private void CBChartData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CReport.Series[0].Points.Clear();
            if (CBChartData.SelectedIndex == 0)
            {
                foreach (var c in MainWindow.db.OrderStatus.ToList())
                {
                    if (c.Order.Count() == 0)
                        continue;
                    CReport.Series[0].Points.Add(orders.Where(b => b.StatusId == c.Id).Count()).AxisLabel = c.Name;
                }
            }
            if(CBChartData.SelectedIndex == 1)
            {
                foreach (var c in orders.Where(b=>b.StatusId == 3).Select(b=>b.User).Distinct().ToList())
                {
                    if (c.Order.Count() == 0)
                        continue;
                    CReport.Series[0].Points.Add(c.Order.Count()).AxisLabel = c.FullName;
                }
            }
        }

        private void RBColumn_Click(object sender, RoutedEventArgs e)
        {
            CReport.Series[0].ChartType = DataVis.Charting.SeriesChartType.Column;
        }

        private void RBBar_Click(object sender, RoutedEventArgs e)
        {
            CReport.Series[0].ChartType = DataVis.Charting.SeriesChartType.Bar;
        }

        private void RBPie_Click(object sender, RoutedEventArgs e)
        {
            CReport.Series[0].ChartType = DataVis.Charting.SeriesChartType.Pie;
        }

        private void RBLine_Click(object sender, RoutedEventArgs e)
        {
            CReport.Series[0].ChartType = DataVis.Charting.SeriesChartType.Line;
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void RBTable_Unchecked(object sender, RoutedEventArgs e)
        {
            WNH.Visibility = Visibility.Visible;
            GDeliveryReport.Visibility = Visibility.Hidden;
        }

        private void RBTable_Checked(object sender, RoutedEventArgs e)
        {
            WNH.Visibility = Visibility.Hidden;
            GDeliveryReport.Visibility = Visibility.Visible;
        }
    }
}
