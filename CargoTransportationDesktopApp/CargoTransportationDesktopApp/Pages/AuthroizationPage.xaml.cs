﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for AuthroizationPage.xaml
    /// </summary>
    public partial class AuthroizationPage : Page
    {
        public AuthroizationPage()
        {
            InitializeComponent();
        }

        private void BAuthroize_Click(object sender, RoutedEventArgs e)
        {
            Model.User user = MainWindow.db.User.FirstOrDefault(c=>c.Login == TBLogin.Text && c.Password == PBPassword.Password);
            if(user.RoleId != 1)
            {
                MessageBox.Show("Неопознанный пользователь");
                return;
            }
            if(user is null || user.Role.Name != "Admin")
            {
                MessageBox.Show("Неопознанный пользователь");
                return;
            }
            MainWindow.loggedUser = user;
            NavigationService.Navigate(new MainMenuPage());
        }
    }
}
