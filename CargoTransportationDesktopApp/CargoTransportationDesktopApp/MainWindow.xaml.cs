﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CargoTransportationDesktopApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static HttpClient client = new HttpClient();
        public static Model.CargoTransportationAppDataBaseEntities db = new Model.CargoTransportationAppDataBaseEntities();
        public static Model.User loggedUser;
        public static string WebApiUrl = "https://cargotransportationwebapi.conveyor.cloud";
        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Navigate(new Pages.AuthroizationPage());
        }
    }
}
