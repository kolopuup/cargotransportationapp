﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DeliveryMobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenuPage : ContentPage
    {
        public MainMenuPage()
        {
            InitializeComponent();
            TBDateTime.BindingContext = DateTime.Now;
            Refresh();
        }
        private async void Refresh()
        {
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/GetOrderDeliveryManToday?id={App.loggedUser.Id}");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            List<Model.Order> orderList = JsonConvert.DeserializeObject<List<Model.Order>>(await dataTask);
            LVOrder.ItemsSource = orderList;
        }

        private void BOrderDetails_Clicked(object sender, EventArgs e)
        {
            if(LVOrder.SelectedItem is null)
            {
                DisplayAlert("Select Item", "Error", "OK");
                return;
            }
            Navigation.PushAsync(new OrderDetailPage((Model.Order)LVOrder.SelectedItem));
        }
    }
}