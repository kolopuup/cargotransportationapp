﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DeliveryMobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AuthorizationPage : ContentPage
    {
        public AuthorizationPage()
        {
            InitializeComponent();
        }

        private async void BSucces_Clicked(object sender, EventArgs e)
        {
            BSucces.IsEnabled = false;
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/UserIsExisting?login={TBLogin.Text}&password={TBPassword.Text}");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            Model.User user = JsonConvert.DeserializeObject<Model.User>(await dataTask);
            if (user is null)
            {
                await DisplayAlert("Error", "User is null", "OK");
                return;
            }
            if(user.RoleId != 2)
            {
                await DisplayAlert("Error", "User is null", "OK");
                return;
            }
            BSucces.IsEnabled = true;
            App.loggedUser = user;
            Navigation.PushAsync(new MainMenuPage());
        }
    }
}