﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DeliveryMobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderDetailPage : ContentPage
    {
        Model.Order postOrder;
        public OrderDetailPage(Model.Order order)
        {
            InitializeComponent();
            Refresh();
            postOrder = order;
            this.BindingContext = postOrder;
        }
        private async void Refresh()
        {
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/GetOrderStatusList");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            List < Model.OrderStatus> orderList = JsonConvert.DeserializeObject<List<Model.OrderStatus>>(await dataTask);
            CBStatus.ItemsSource = orderList;
        }
        private async void BChangeStatus_Clicked(object sender, EventArgs e)
        {
            (int, int) OrderIdStatusId;
            OrderIdStatusId.Item1 = postOrder.Id;
            OrderIdStatusId.Item2 = ((Model.OrderStatus)CBStatus.SelectedItem).Id;
            string json = JsonConvert.SerializeObject(OrderIdStatusId);
            HttpContent content = new StringContent(@json, Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponseMessage = await App.client.PutAsync($"{App.ConnectionIP}/api/EditOrderStatus", content);
            await DisplayAlert("Status", "Статус изменён", "Ok");
            await Navigation.PopAsync();
        }
    }
}