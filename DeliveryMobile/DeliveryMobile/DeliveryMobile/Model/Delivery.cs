﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliveryMobile.Model
{
    public class Delivery
    {
        public int Id { get; set; }
        public double PackageWeight { get; set; }
        public int PackageTypeId { get; set; }
        public Nullable<int> CityId { get; set; }
        public Nullable<int> StreetId { get; set; }
        public Nullable<int> HouseNumber { get; set; }
        public Nullable<int> FlatNumber { get; set; }
        public int SpeedTypeId { get; set; }
        public Nullable<int> PickUpPointId { get; set; }
        public int FromWhomId { get; set; }
        public string ForWhomFullName { get; set; }
        public double Cost { get; set; }

        public virtual City City { get; set; }
        public virtual PackageType PackageType { get; set; }
        public virtual PickUpPoint PickUpPoint { get; set; }
        public virtual SpeedType SpeedType { get; set; }
        public virtual Street Street { get; set; }
        public virtual User User { get; set; }
    }
}
