﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliveryMobile.Model
{
    public class SpeedType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ValueAdditionPercentage { get; set; }
    }
}
