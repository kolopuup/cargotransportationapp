﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliveryMobile.Model
{
    public class PackageType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Cost { get; set; }
    }
}
