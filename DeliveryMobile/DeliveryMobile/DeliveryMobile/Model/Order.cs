﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliveryMobile.Model
{
    public class Order
    {
        public int Id { get; set; }
        public int DeliveryId { get; set; }
        public int DeliveryManId { get; set; }
        public int StatusId { get; set; }
        public string TrackNumber { get; set; }
        public System.DateTime EstimatedDeliveryTime { get; set; }

        public virtual Delivery Delivery { get; set; }
        public virtual OrderStatus OrderStatus { get; set; }
        public virtual User User { get; set; }
        [JsonIgnore]
        public bool IsCompleted { get; set; }
        [JsonIgnore]
        public string FullAddress 
        {
            get
            {
                if(Delivery.PickUpPoint is null)
                {
                    return Delivery.City.Name + " " + Delivery.Street.Name + " " + Delivery.HouseNumber + " " + Delivery.FlatNumber;
                }
                else
                {
                    return "Pick Up Point: " + Delivery.PickUpPoint.City.Name + " " + Delivery.PickUpPoint.Street.Name + " " + Delivery.PickUpPoint.HouseNumber + " " + Delivery.FlatNumber;
                }
            }
            set
            {
                
            }
        }
    }
}
