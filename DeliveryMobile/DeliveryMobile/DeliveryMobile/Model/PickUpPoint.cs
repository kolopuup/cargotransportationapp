﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliveryMobile.Model
{
    public class PickUpPoint
    {
        public int Id { get; set; }
        public int CityId { get; set; }
        public int StreetId { get; set; }
        public int HouseNumber { get; set; }

        public virtual City City { get; set; }
        public virtual Street Street { get; set; }
    }
}
