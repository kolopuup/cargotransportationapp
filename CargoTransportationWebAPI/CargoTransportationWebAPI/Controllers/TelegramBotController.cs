﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using System.Threading.Tasks;

namespace CargoTransportationWebAPI.Controllers
{
    public class TelegramBotController : ApiController
    {
        // GET: api/TelegramBot
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TelegramBot/5
        [Route("api/GetPushNotifyStatus")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TelegramBot
        [Route("api/PostNewOrder")]
        public async void PostNewOrder(int OrderId)
        {
            Models.Order order = HomeController.db.Order.FirstOrDefault(c => c.Id == OrderId);
            Models.User user = order.Delivery.User;
            if (user.TelegramAccount == null)
                return;
            await HomeController.client.SendTextMessageAsync(user.TelegramAccount, $"Здравствуйте {user.FullName}!\nВаш заказ отправлен на обработку\nТрек Номер: {order.TrackNumber}");
        }

        // PUT: api/TelegramBot/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TelegramBot/5
        public void Delete(int id)
        {
        }
    }
}
