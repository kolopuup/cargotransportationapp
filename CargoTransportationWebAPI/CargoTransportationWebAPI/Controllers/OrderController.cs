﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CargoTransportationWebAPI.Controllers
{
    public class OrderController : ApiController
    {
        // GET: api/Order
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Order/5
        [Route("api/GetOrderDeliveryManToday")]
        public List<Models.Order> GetOrderDeliveryManToday(int id)
        {
            return HomeController.db.Order.Where(c=>c.DeliveryManId == id && c.EstimatedDeliveryTime.Day == DateTime.Now.Day && c.EstimatedDeliveryTime.Month == DateTime.Now.Month && c.EstimatedDeliveryTime.Year == DateTime.Now.Year).ToList();
        }
        [Route("api/GetOrderStatusList")]
        public List<Models.OrderStatus> GetOrderStatusList()
        {
            List<Models.OrderStatus> orderStatuses = HomeController.db.OrderStatus.ToList();
            return orderStatuses;
        }
        // POST: api/Order
        [Route("api/PostOrderDeliveryManToday")]
        public void Post([FromBody]string value)
        {

        }
        [HttpPut]
        [Route("api/EditOrderStatus")]
        // PUT: api/Order/5
        public void Put(HttpRequestMessage request)
        {
            (int, int) order = JsonConvert.DeserializeObject<(int, int)>(request.Content.ReadAsStringAsync().Result);
            Models.Order dbOrder = HomeController.db.Order.FirstOrDefault(c => c.Id == order.Item1);
            dbOrder.StatusId = order.Item2;
            Models.User user = dbOrder.Delivery.User;
            HomeController.db.SaveChanges();
            if(user.TelegramAccount != null)
            {
                HomeController.client.SendTextMessageAsync(user.TelegramAccount, $"Статус заказа изменился\nTrackNumber: {dbOrder.TrackNumber}\nСтатус: {dbOrder.OrderStatus.Name}");
            }
        }

        // DELETE: api/Order/5
        public void Delete(int id)
        {
        }
    }
}
