﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telegram.Bot;
using Telegram.Bot.Args;
using System.Threading.Tasks;
using System.Net.Http;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using System.Diagnostics;

namespace CargoTransportationWebAPI.Controllers
{
    public class HomeController : Controller
    {
        public static Models.CargoTransportationAppDataBaseEntities db = new Models.CargoTransportationAppDataBaseEntities();
        private const string token = "5342626658:AAHYmZpxuZ6lCB0sUoQYH0H4mOmKafMHryU";
        public static TelegramBotClient client;
        public ActionResult Index()
        {
            client = new TelegramBotClient(token);
            client.OnMessage += Client_OnMessage;
            Task.Run(() => StartReceiving());
            ViewBag.Title = "Home Page";

            return View();
        }
        private async Task StartReceiving()
        {
            client.StartReceiving();
        }

        private async void Client_OnMessage(object sender, MessageEventArgs e)
        {
            string MessageChatId = Convert.ToString(e.Message.Chat.Id);
            Debug.WriteLine(MessageChatId);
            if (db.User.FirstOrDefault(c => c.TelegramAccount == MessageChatId) != null)
            {


                Models.User user = db.User.FirstOrDefault(c => c.TelegramAccount == MessageChatId);
                
                if(user.TelegramStatusId == 2)
                {
                    Models.Order order = db.Order.FirstOrDefault(c => c.TrackNumber == e.Message.Text && c.Delivery.FromWhomId == user.Id);
                    if(order is null)
                    {
                        client.SendTextMessageAsync(e.Message.Chat.Id, "Посылка с данным трек номером не найдена");
                        user.TelegramStatusId = 1;
                        db.SaveChanges();
                        return;
                    }
                    if(order.Delivery.PickUpPoint is null)
                    {
                        client.SendTextMessageAsync(e.Message.Chat.Id, $"Место получения: г.{order.Delivery.City.Name} ул.{order.Delivery.Street.Name} д.{order.Delivery.HouseNumber} кв.{order.Delivery.FlatNumber}\nСтатус заказа: {order.OrderStatus.Name}\nПредполагаемая дата доставки: {order.EstimatedDeliveryTime.Year}/{order.EstimatedDeliveryTime.Month}/{order.EstimatedDeliveryTime.Day}");
                        user.TelegramStatusId = 1;
                        db.SaveChanges();
                        return;
                    }
                    else
                    {
                        client.SendTextMessageAsync(e.Message.Chat.Id, $"Место получения: Точка самовывоза г.{order.Delivery.PickUpPoint.City.Name} ул.{order.Delivery.PickUpPoint.Street.Name} д.{order.Delivery.PickUpPoint.HouseNumber}\nСтатус заказа: {order.OrderStatus.Name}\nПредполагаемая дата доставки: {order.EstimatedDeliveryTime.Year}/{order.EstimatedDeliveryTime.Month}/{order.EstimatedDeliveryTime.Day}");
                        user.TelegramStatusId = 1;
                        db.SaveChanges();
                        return;
                    }
                }

                if (e.Message.Text == "/start")
                {
                    client.SendTextMessageAsync(e.Message.Chat.Id, "Вы уже авторизованы");
                    return;
                }
                if (e.Message.Text == "/exit")
                {
                    user.TelegramStatusId = null;
                    user.TelegramAccount = null;
                    db.SaveChanges();
                    client.SendTextMessageAsync(e.Message.Chat.Id, "Выход успешно проведён");
                    return;
                }
                if(e.Message.Text == "/tracknumber")
                {
                    client.SendTextMessageAsync(e.Message.Chat.Id, "Введите трек номер");
                    user.TelegramStatusId = 2;
                    db.SaveChanges();
                    return;
                }
                if(e.Message.Text == "/allmyorders")
                {
                    List<Models.Order> orders = db.Order.Where(c => c.Delivery.User.Id == user.Id).ToList();
                    if(orders.Count == 0)
                    {
                        await client.SendTextMessageAsync(e.Message.Chat.Id, $"У вас пока что нет заказов");

                    }
                    foreach (var c in orders)
                    {
                        if (c.Delivery.PickUpPoint is null)
                        {
                            await client.SendTextMessageAsync(e.Message.Chat.Id, $"Трек номер: {c.TrackNumber}\nМесто получения: г.{c.Delivery.City.Name} ул.{c.Delivery.Street.Name} д.{c.Delivery.HouseNumber} кв.{c.Delivery.FlatNumber}\nСтатус заказа: {c.OrderStatus.Name}\nПредполагаемая дата доставки: {c.EstimatedDeliveryTime.Year}/{c.EstimatedDeliveryTime.Month}/{c.EstimatedDeliveryTime.Day}");
                        }
                        else
                        {
                            await client.SendTextMessageAsync(e.Message.Chat.Id, $"Трек номер: {c.TrackNumber}\nМесто получения: Точка самовывоза г.{c.Delivery.PickUpPoint.City.Name} ул.{c.Delivery.PickUpPoint.Street.Name} д.{c.Delivery.PickUpPoint.HouseNumber}\nСтатус заказа: {c.OrderStatus.Name}\nПредполагаемая дата доставки: {c.EstimatedDeliveryTime.Year}/{c.EstimatedDeliveryTime.Month}/{c.EstimatedDeliveryTime.Day}");
                        }
                    }
                }
            }



            if(db.User.FirstOrDefault(c => c.TelegramAccount == MessageChatId) == null)
            {

                if (e.Message.Text == "/start" && e.Message.Text != null && db.User.FirstOrDefault(c => c.TelegramAccount == MessageChatId) is null)
                {
                    client.SendTextMessageAsync(e.Message.Chat.Id, "Для использования бота вам необходимо авторизироваться\nВведите логин пароль в формате:\nlogin:password\nИли зарегестрируйтесь с помощью команды /registration");
                    return;
                }

                if (e.Message.Text.Contains("/registration") && e.Message.Text != null)
                {
                    client.SendTextMessageAsync(e.Message.Chat.Id, "Введите данные через пробел:\nFullName Login Password Email PhoneNumber");
                    return;
                }

                if (e.Message.Text.Contains(" "))
                {
                    string FullName;
                    string Login;
                    string Password;
                    string Email;
                    string PhoneNumber;
                    string[] split = e.Message.Text.Split(' ');
                    FullName = split[0];
                    Login = split[1];
                    Password = split[2];
                    Email = split[3];
                    PhoneNumber = split[4];
                    Models.User user = new Models.User() { FullName = FullName, Login = Login, Password = Password, Email = Email, PhoneNumber = PhoneNumber, RoleId = 4, TelegramAccount = MessageChatId, TelegramStatusId = 1};
                    HomeController.db.User.Add(user);
                    HomeController.db.SaveChanges();
                    client.SendTextMessageAsync(e.Message.Chat.Id, $"{user.FullName}, ваша регистрация прошла успешно! Не забудьте логин и пароль.");
                    return;

                }
                if (e.Message.Text.Contains(":"))
                {
                    string login;
                    string password;
                    string[] split = e.Message.Text.Split(':');
                    login = split[0];
                    password = split[1];
                    Models.User user = db.User.FirstOrDefault(c => c.Login == login && c.Password == password);
                    if(user is null)
                    {
                        client.SendTextMessageAsync(e.Message.Chat.Id, "Пользователь не найден");
                        return;
                    }
                    if(user != null)
                    {
                        user.TelegramStatusId = 1;
                        user.TelegramAccount = MessageChatId;
                        db.SaveChanges();
                        client.SendTextMessageAsync(e.Message.Chat.Id, $"{user.FullName} поздравялем вас с авторизацией");
                    }
                    return;
                }
            }
        }
    }
}
